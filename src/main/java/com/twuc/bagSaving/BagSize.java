package com.twuc.bagSaving;

public enum BagSize {
    MINI(10),
    SMALL(20),
    MEDIUM(30),
    BIG(40),
    HUGE(50);

    private final int sizeNumber;

    BagSize(int sizeNumber) {
        this.sizeNumber = sizeNumber;
    }

    int getSizeNumber() {
        return sizeNumber;
    }
}
